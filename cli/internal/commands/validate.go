package commands

import (
	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/config"
	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/validator"

	"github.com/urfave/cli/v2"
)

// Validate runs validation on current configuration.
func Validate() *cli.Command {
	return &cli.Command{
		Name:  "validate",
		Usage: "Validate configuration",
		Action: func(ctx *cli.Context) error {
			return validate(ctx)
		},
	}
}

func validate(ctx *cli.Context) error {
	cfg, err := config.GetConfig(ctx)
	if err != nil {
		return err
	}

	err = validator.Validate(cfg)
	if err != nil {
		return err
	}

	return nil
}
