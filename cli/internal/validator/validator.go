package validator

import (
	"bytes"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/config"
)

// ErrorMap is a map which contains all errors from validating a struct.
type ErrorMap map[string]ErrorArray

// ErrorMap implements the Error interface so we can check error against nil.
// The returned error is all existing errors with the map.
func (err ErrorMap) Error() string {
	var b bytes.Buffer

	for k, errs := range err {
		if len(errs) > 0 {
			b.WriteString(fmt.Sprintf("%s: %s, ", k, errs.Error()))
		}
	}

	return strings.TrimSuffix(b.String(), ", ")
}

// ErrorArray is a slice of errors returned by the Validate function.
type ErrorArray []error

// ErrorArray implements the Error interface and returns all the errors comma seprated
// if errors exist.
func (err ErrorArray) Error() string {
	var b bytes.Buffer

	for _, errs := range err {
		b.WriteString(fmt.Sprintf("%s, ", errs.Error()))
	}

	errs := b.String()

	return strings.TrimSuffix(errs, ", ")
}

// ValidationFunc is a function that receives a pointer to
// a configuration struct, runs validation on a given configuration
// value, and returns an error upon failure or nil upon success.
type ValidationFunc func(cfg *config.Config) error

// Validator implements a validator.
type Validator struct {
	ValidationFuncs map[string]ValidationFunc
}

// NewValidator creates a new Validator.
func NewValidator() *Validator {
	return &Validator{
		ValidationFuncs: make(map[string]ValidationFunc),
	}
}

// validator defines a new validator populated by each
// "validate_*.go" validation file's init() function.
var v = NewValidator()

// SetValidationFunc adds a validation test for a given field.
func SetValidationFunc(name string, vf ValidationFunc) error {
	return v.SetValidationFunc(name, vf)
}

// SetValidationFunc adds a validation test for a given field.
func (v *Validator) SetValidationFunc(name string, vf ValidationFunc) error {
	if name == "" {
		return errors.New("name cannot be empty")
	}

	v.ValidationFuncs[name] = vf

	return nil
}

// Validate executes all validation tests.
// All validation checks will be run before an error is returned, even if
// there are multiple validation failures.
func Validate(cfg *config.Config) error {
	return v.Validate(cfg)
}

// Validate executes all validation tests.
// All validation checks will be run before an error is returned, even if
// there are multiple validation failures.
func (v *Validator) Validate(cfg *config.Config) error {
	m := make(ErrorMap)

	for name, validationFunc := range v.ValidationFuncs {
		var errs ErrorArray

		if err := validationFunc(cfg); err != nil {
			if errarr, ok := err.(ErrorArray); ok {
				errs = errarr
			} else if err != nil {
				errs = ErrorArray{err}
			}

			m[name] = errs
		}
	}

	if len(m) > 0 {
		return m
	}

	return nil
}
