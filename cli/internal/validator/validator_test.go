package validator_test

import (
	"errors"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/config"
	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/validator"
)

var tests = []struct {
	v             *validator.Validator
	name          string
	fn            validator.ValidationFunc
	expectedError string
}{
	{
		v:    validator.NewValidator(),
		name: "option1",
		fn: func(cfg *config.Config) error {
			if cfg.Options.Option1 == "" {
				return errors.New("option1 should be set")
			}

			return nil
		},
		expectedError: "option1: option1 should be set",
	},
	{
		v:    validator.NewValidator(),
		name: "secret2",
		fn: func(cfg *config.Config) error {
			if cfg.Secrets.Secret2 == "" {
				return errors.New("secret2 should be set")
			}

			return nil
		},
		expectedError: "secret2: secret2 should be set",
	},
	{
		v:    validator.NewValidator(),
		name: "option2",
		fn: func(cfg *config.Config) error {
			if cfg.Options.Option2 != "" {
				return errors.New("option2 should be empty")
			}

			return nil
		},
		expectedError: "",
	},
}

func TestSetValidationFunc(t *testing.T) {
	test := tests[0]

	if err := test.v.SetValidationFunc(test.name, test.fn); err != nil {
		t.Fatalf("SetValidationFunc() error: %v", err)
	}

	want := validator.Validator{
		ValidationFuncs: map[string]validator.ValidationFunc{
			test.name: test.fn,
		},
	}

	got := test

	if reflect.DeepEqual(want, got) {
		t.Errorf("expected %v but got %v", want, got)
	}
}

func TestValidate(t *testing.T) {
	// Test each validator individually.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.v.SetValidationFunc(tt.name, tt.fn); err != nil {
				t.Fatalf("SetValidationFunc() error: %v", err)
			}
			cfg := config.NewConfig()

			var got string
			result := tt.v.Validate(cfg)
			if result == nil {
				got = ""
			} else {
				got = result.Error()
			}
			want := tt.expectedError

			if got != want {
				t.Errorf("expected %s but got %s", got, want)
			}
		})
	}

	// Test all validations together.
	t.Run("all validations", func(t *testing.T) {
		var v = validator.NewValidator()

		for _, tt := range tests {
			if err := v.SetValidationFunc(tt.name, tt.fn); err != nil {
				t.Fatalf("SetValidationFunc() error: %v", err)
			}
		}

		cfg := config.NewConfig()

		got := v.Validate(cfg).Error()

		// Need to define desired output as a slice of strings and call
		// `testSliceContains()` because slices do not guarantee order.
		want := []string{
			"option1: option1 should be set",
			"secret2: secret2 should be set",
		}

		assertContains(t, got, want)
	})
}

func assertContains(t *testing.T, reference string, values []string) {
	t.Helper()

	for _, value := range values {
		if !strings.Contains(reference, value) {
			t.Errorf("%s did not contain %s", reference, value)
		}
	}
}
