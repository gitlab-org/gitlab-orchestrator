package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/urfave/cli/v2"
)

// Config represents all Options and Secrets required for Orchestrator.
type Config struct {
	Options `json:"options"`
	Secrets `json:"secrets"`
}

// Options represents a collection of configuration options.
type Options struct {
	Option1 string `json:"option1"`
	Option2 string `json:"option2"`
}

// Secrets represent a collection of secrets.
type Secrets struct {
	Secret1 string `json:"secret1"`
	Secret2 string `json:"secret2"`
}

// NewConfig returns a new Config struct.
// Default values can be set here.
func NewConfig() *Config {
	return &Config{}
}

// GetConfig returns current configuration.
func GetConfig(ctx *cli.Context) (*Config, error) {
	cfg := NewConfig()

	optionsFile := ctx.String("options-file")
	if err := loadFromJSON(optionsFile, &cfg.Options); err != nil {
		return nil, err
	}

	secretsFile := ctx.String("secrets-file")
	if err := loadFromJSON(secretsFile, &cfg.Secrets); err != nil {
		return nil, err
	}

	return cfg, nil
}

func loadFromJSON(file string, target interface{}) error {
	if _, err := os.Stat(file); err == nil {
		content, err := ioutil.ReadFile(filepath.Clean(file))
		if err != nil {
			return err
		}

		err = json.Unmarshal(content, &target)
		if err != nil {
			return err
		}
	}

	return nil
}
