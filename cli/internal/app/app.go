package app

import (
	"log"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/commands"
)

const (
	appName  = "orchestrator"
	appUsage = "A project to orchestrate installations of GitLab for internal testing, customer sites, and any other applicable use cases."
	appHelp  = `
{{.Name}} - {{.Usage}}

Usage: {{.Name}} {{if .VisibleFlags}}[global options]{{end}}{{if .Commands}} command [command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}

The available commands for execution are listed below.

Commands:
{{range .Commands}}{{if not .HideHelp}}{{ "\t"}}{{join .Names ", "}}{{ "\t"}}{{.Usage}}{{ "\n" }}{{end}}{{end}}

Global options:
{{- range .VisibleFlags}}
{{ "\t"}}{{.}}
{{end}}
`
	commandHelp = `
{{.Name}} - {{.Usage}}

Usage: orchestrator {{.Name}} {{if .VisibleFlags}}[command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}

Options:
{{- range .VisibleFlags}}
{{ "\t"}}{{.}}
{{end}}
`
)

// New creates cli.App for the Orchestrator CLI.
func New(logInfo, logError *log.Logger) *cli.App {
	cli.AppHelpTemplate = appHelp
	cli.CommandHelpTemplate = commandHelp

	return &cli.App{
		Name:  appName,
		Usage: appUsage,
		Commands: []*cli.Command{
			commands.Init(),
			commands.Validate(),
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "options-file",
				Usage:    "Provide configuration from a JSON file.",
				Required: false,
				Aliases:  []string{"f"},
				EnvVars:  []string{"OPTIONS_FILE"},
				Value:    "options.json",
			},
			&cli.StringFlag{
				Name:     "secrets-file",
				Usage:    "Provide secrets from a JSON file.",
				Required: false,
				Aliases:  []string{"s"},
				EnvVars:  []string{"SECRETS_FILE"},
				Value:    "secrets.json",
			},
		},
	}
}
