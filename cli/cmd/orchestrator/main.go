package main

import (
	"log"
	"os"

	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/app"
)

func main() {
	logInfo := log.New(os.Stderr, "orchestrator | [ INFO ] ", log.Ldate|log.Ltime)
	logError := log.New(os.Stderr, "orchestrator | [ ERROR ] ", log.Ldate|log.Ltime)

	if err := app.New(logInfo, logError).Run(os.Args); err != nil {
		logError.Fatal(err)
	}
}
