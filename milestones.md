# Orchestrator Project Milestones

- ***2020-04-16*** Orchestrator gains ability to deploy multiple multi-node
  clusters.
- ***2020-03-21*** First code release cut from `gitlab-provisioner` project.
