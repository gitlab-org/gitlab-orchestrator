# Workflow

**Orchestrator** is under rapid development. The chart below describes at a
high level how a developer would interact with the current tool.

```mermaid
stateDiagram
  state Initialization {
      GetToken --> ConfigureSecrets
      ConfigureSecrets --> BuildContainer
      BuildContainer --> RunContainer
  }
  note right of Initialization
    - Secrets placed in secrets/user_config.sh
    - scripts/build-dev-container.sh
    - scripts/run-dev-container.sh
  end note
  state Provisioning {
      TerraformCreate --> ExtractInventory
      ExtractInventory --> ExtraVarsCreate
  }
  note right of Provisioning : provision-cluster
  state InventoryAssembly {
      GenerateNodeInventory --> IdentifySSHKeys
  }
note right of InventoryAssembly : assemble-inventory
  state Orchestrating {
      AnsibleRun
  }
  note right of Orchestrating : run-orchestration
  state Deprovisioning {
      TerraformDestroy --> CleanUpLocal
  }
  state Exit {
      ExitContainer
  }
  note right of Deprovisioning : deprovision-cluster
  Initialization --> Provisioning
  Provisioning --> InventoryAssembly
  InventoryAssembly --> Orchestrating
  Orchestrating --> Deprovisioning
  Deprovisioning --> Exit
```
