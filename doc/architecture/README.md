# Orchestrator Architecture

## Designs

- [Architectural Vision](vision.md)

## Core components

**Orchestrator** leverages the combined strength of **Terraform**, **Ansible**, and the **Omnibus** package to provision, configure, and upgrade GitLab at scale. It is a policy engine implementing the [GitLab reference architectures](https://docs.gitlab.com/ee/administration/reference_architectures/) and eliminates the manual deployment steps.

[Technical rationales for each selected component is written down in the public decision records](decisions/0012_mission_and_core_components.md).

## Architectural Decision Records

- [0001: Product Name is Orchestrator](decisions/0001_product_name_is_orchestrator.md)
- [0002: User Experience Targets: Alpha Release](decisions/0002_user_experience_targets_alpha_release.md)
- [0003. Architectural Decision Record Format](decisions/0003_architectural_decision_format.md)
- [0004. Methods for Handling Secrets](decisions/0004_methods_for_handling_secrets.md)
- [0005. Orchestrator Release Process](decisions/0005_orchestrator_release_process.md)
- [0006. Database Security Defaults](decision/0006_database_security_defaults.md)
- [0007. Command Line User Interface](decisions/0007-command-line-user-interface.md)
- [0008. Ansible Modules and Plugins](decisions/0008_ansible_modules_and_plugins.md)
- [0009. Command Line User Interface](decisions/0009-command-line-user-interface.md)
- [0010. Inventory Grouping and Variable Presentation](decisions/0010_inventory_grouping_and_variable_presentation.md)
- [0011. Validating Configuration](0011_validating_configuration.md)
- [0012. Mission and Core Components](0012_mission_and_core_components.md)
