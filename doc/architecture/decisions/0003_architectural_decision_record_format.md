# 0003. Architectural Decision Record Format

- Proposed: 2020-07-28
- Accepted: 2020-07-29

## Status

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/259)

1. Need to track decisions including when our [two-way door policy](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions) is used to make changes.
1. Structured format will make it easier to follow the decision tree.

## Decision

1. Manual format in markdown where this example is in `/doc/architecture/decisions/0001_my_decision_record.md`
    ```markdown
    # 0001. My Decision Record

    Proposed: YYYY-MM-DD
    Accepted|Rejected: YYYY-MM-DD
    Deprecated|Superceded: YYYY-MM-DD

    ## Status

    Accepted|Proposed|Rejected|Deprecated|Superceded

    ## Context

    Link to initial issue to discuss

    Prose describing contexts and constraints that impacted the decision making process.

    ## Decision

    Document the outcome of the change proposal.

    ## Consequences

    List tradeoffs made in this decision.
    ```
1. Track superseded items manually in `/doc/architecture/decisions/metadata.json`
    ```json
    {
        "superseded": {
            new_decision: old_decision
        }
    }
    ```

## Consequences

1. Start documenting in a more structured way immediately.
1. Lack of tooling means we have a potential for typos or errors. [Mitigate this with tooling updates](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/265)
