# Orchestrator Release Process

- Proposed: 2020-07-31
- Accepted: 2020-08-13

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/216)

## Decision

1. [Release Orchestrator using Semantic Versioning](https://semver.org/)

    | Phase | Orchestrator | Note |
    | :-- | :-- | :-- |
    | Minimum Viable | 0.0.1 - 0.0.9 | Proof of concept work |
    | Alpha | 0.1.0 - 0.m.z | Rapidly developed releases working through Alpha features|
    | Beta | 0.n.0 - 0.p.z | Releases refining the product until ready for first Generally Available release |
    | GA | 1.0.0+ | First public release of product  |

1. Orchestrator branch names support maintenance of semantic versions

    | Development Phase | Branch Name | Tag Name |
    | :-- | :-- | :-- |
    | Primary Release | `Major.Minor-stable` | `Major.Minor.Patch` |
    | Maintenance Release | `Major.Minor-stable` | `Major.Minor.Patch` |
    | Development | `master` | **No Tags Applied** |

1. Orchestrator and GitLab upstream release cycles are not aligned creating [consequences for communication](#consequences)
1. Orchestrator releases document which GitLab releases they can deploy and
   the upgrade path

    | Orchestrator Version | Installs GitLab Versions | Upgrades from GitLab Versions |
    | :-- | :-- | :-- |
    | `Major.Minor` | `Major.Minor`, `Major.Minor`, ... | `Major.Minor`, `Major.Minor` |

1. **Orchestrator** follows the [GitLab deprecation policy for configuration and features](https://docs.gitlab.com/omnibus/package-information/deprecation_policy.html) after exiting from [Beta and moving into General Availability](https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha-beta-ga)
1. Orchestrator provides different artifacts for developers and consumers
1. Containers are OCI complaint and support [PodMan](https://podman.io/) and Docker runtimes
1. Containers are tagged per each release
    - `BRANCH_SLUG` is the branch name as represented in GitLab pipelines by [`CI_COMMIT_REF_SLUG`](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

    | Container Type | Image Tag |
    | :-- | :-- |
    | developer | `orchestrator/developer:BRANCH_SLUG` |
    | production | `orchestrator:Major.Minor.Patch` |
1. Consumer releases include the command line binary and a container
    - Command line binary downloads the container for the user if required
    - Consumer container encapsulates all tools and code
1. Developer releases include the command line binary and a container
    - Command line binary downloads the container for the user if required
    - Developer container encapsulates the tools, but the code is mounted
      from the local disk enabling a faster read-eval-print loop

## Consequences

1. Orchestrator is able to move and react quickly to upstream changes in
   technology or features.
1. Unaligned releases may confuse end users. Mitigated through compatibility
   matrix.
