# Database Security Defaults

- Proposed: 2020-07-31
- Accepted: 2020-08-07

## Status

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/209)

## Decision

1. Encrypt all connections made to the database from external systems using
   **SSL** encryption
1. Verify the identity of all connections using `auth`
1. Limit trusted CIDR lists to nodes with **Rails** services
    > Exception made for Praefect nodes if database is deployed with the
    > Rails database.
    - SideKiq
    - Geo-LogCursor
    - GitLab-Exporter
    - Rails/WebService
    - Task-Runner
1. Specify the network interface listening for database connections rather
   than using the generic `0.0.0.0`
1. Migrate to **[SCRAM](https://www.postgresql.org/docs/12/sasl-authentication.html)**
   instead of using **MD5**

## Consequences

1. Increased security posture benefits consumers of GitLab.
1. Encrypting connections introduces complexity for configuration.
