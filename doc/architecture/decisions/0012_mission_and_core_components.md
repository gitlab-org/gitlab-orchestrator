# Mission and Core Components

- Proposed: 2019-08-02
- Accepted: 2019-10-07

## Status

Accepted

## Context

- [Productization Discussion](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/314)
- [Feature Gap Review](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/2)

## Decision

1. **Orchestrator** will integrate **Ansible** and **Terraform** as the base
   for its core functionality.
    - **Terraform** excels in provisioning infrastructure, but has limited
      capabilities for continuing configuration of services.
    - **Ansible** excels at configuration management, is pure open source,
      and has already been vetted for various customer types.
    - Professional Services, the Geo Team, Quality Assurance, and
      Distribution have independently decided to use the same tools.
    - **Hashicorp** and **Red Hat** have [written white papers](https://www.redhat.com/cms/managed-files/pa-terraform-and-ansible-overview-f14774wg-201811-en.pdf)
      and [co-presented on the strength of combining **Ansible** and **Terraform**](https://www.hashicorp.com/resources/ansible-terraform-better-together).
1. **Orchestrator** will create multi-site deployments through installing
   the existing **Omnibus** package.
1. **Orchestrator** will generate and install `gitlab.rb` files for each
   component.
1. **Orchestrator** will execute installation and upgrade steps for
   multi-site deployments including but not limited to:
    - running `gitlab-ctl reconfigure` as appropriate
    - setting up database replication
    - installing cryptographic keys
    - configuring Geo secondary sites
    - installation and update of GitLab licenses

## Consequences

1. Simplifies the installation of Omnibus at scale.
1. Acts as a policy engine for the reference architectures.
1. Standard installation practices reduces complexity for Support.
1. Increases testing capabilities for internal teams.
1. Converges internal effort on one set of tools that follow established
   best practices that align to what customers use.
