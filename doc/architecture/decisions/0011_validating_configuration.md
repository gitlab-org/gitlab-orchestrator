# Validating Configuration

- Proposed: 2020-09-24
- Accepted: 2020-10-07

## Status

Approved

## Context

- [Review Discussion](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/287)

## Decision

1. Orchestrator configuration will be well defined in an internal `config` structure. Runtime execution populates this structure by merging configuration files, secrets, command line flags, and environment variables.
1. Validation will run on the entire Config struct when using the `validate` and `apply` commands. In the case of `set`, validation will only be run on the variable being set. This will avoid unhelpful errors about required variables while the user is in the process of loading the variables.
1. Validation routines will be handled using [the factory pattern](https://en.wikipedia.org/wiki/Factory_method_pattern). Validators are independent of each other and are registered to a central control mechanism. Developers may add new validators by simply adding a new validator definition code file.
1. **Orchestrator** will keep default configurations in their own separate file. Runtime execution will merge this with other data following this priority from highest to lowest:
    1. command line options
    1. secrets
    1. user configuration
    1. default settings
1. Avoid if/else scenarios in the configuration files and instead define dependencies between the validator factory methods. For example, use `bucket` instead of `gcs_bucket` and `s3_bucket` and bucket will depend upon a validator suggested by the provider validator.

## Consequences

1. This approach validates configuration by validating each option itself in isolation. If there are connections between configuration options we will have to write our custom code to handle this. For example: if `redis_nodes` is only required when `redis_ha` is set to `true`. Marking conditional options as required will require custom code.
