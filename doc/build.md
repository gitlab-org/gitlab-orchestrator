# Building Orchestrator

## Instructions to build the CLI

Follow these instructions to create the binary for Orchestrator's CLI.

1. `cd cli`
1. Run `make help` to see a list of available options.
1. Run `make` or `make build` to build the Go binary.
1. Run `./orchestrator help` to see the help output.
