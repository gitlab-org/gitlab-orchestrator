# Original Orchestrator Project Proposal

The chart below is taken from [the initial proposal to make the
`gitlab-provisioner` more extensible](https://gitlab.com/gitlab-org/gitlab-orchestrator/issues/3).
The linked issue provides a large amount of detail about the rationale for
this architectural design.

```mermaid
graph LR;
  tf -->|provisioning request|cloudapi

  subgraph "Orchestration"
    orch[ansible] -->|query hosts|tfinv[Inventory Parser]
  end

  subgraph "Cloud Provider"
    cloudapi[cloud service provider] -->|request creation|cluster[provisioned nodes]
  end

  subgraph "Provisioned State"
    tfinv[Inventory Parser] -->|read state|provstate
    provstate[node state data]
  end

  subgraph "Provisioner"
    tf[terraform] -->|transmit state data|tfstate[terraform state data store]
    tf -->|write state|provstate
  end
```
