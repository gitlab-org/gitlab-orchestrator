### This project is deprecated in favor of the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit). 

---

# GitLab Orchestrator

## Overview

GitLab Orchestrator provides an automated, repeatable method to install,
configure and upgrade GitLab using Omnibus packages. Orchestrator eliminates
most of the manual steps required for large deployments across multiple
nodes including advanced features such as Geo, load balancing for multiple
rails nodes, clustered databases, and **consul**.

It is ***not*** yet recommended for production usage, though interested parties are welcome to
suggest features and contribute fixes.

## Learn about the Orchestrator

- [Quickstart Guide](doc/quickstart.md)
- [Workflow Overview](doc/workflow.md)
- [Project History](doc/history.md)
- [Known Issues and Troubleshooting](doc/troubleshooting.md)
- [Planning Methodology](doc/planning.md)
- [Contributor's Guide](CONTRIBUTING.md)
- [Architectural Design and Decisions](doc/architecture/README.md)

## Current Happenings

- **Next Group Conversation: Wednesday August 19th, 2020 at 16:00 UTC**
- [Work in Progress: Geo+HA Deployment Automation Epic](https://gitlab.com/groups/gitlab-org/-/epics/2376).
- [Geo Minimum Product is ready for testing]( https://gitlab.com/gitlab-org/gitlab-orchestrator/-/merge_requests/8)
- [Big Provision Refactor](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/143)
- [Orchestrator Moving to **Go** based cross-platform CLI](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/29)
