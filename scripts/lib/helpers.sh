# shellcheck shell=bash
fail() {
    echo "${1}" >&2
    exit 1
}

file_exists() {
    [ -f "${1}" ] || fail "${1} does not exist"
}

directory_exists() {
    [ -d "${1}" ] || fail "${1} is not a directory"
}

validate_variable() {
    [ -n "${!1}" ] || fail "${1} is empty"
}

# From https://gist.github.com/esycat/5279354
portable_readlink() {
    local target="${1}"

    # requird to deal with quoted values in RC files
    target_dir="$(dirname "${target}")"
    if [ "${target_dir}" = "~" ]; then
        target_dir="${HOME}"
    fi

    cd "${target_dir}" || fail "Bad directory: ${target}"
    target=$(basename "${target}")

    # Iterate down a (possible) chain of symlinks
    while [ -L "${target}" ]; do
        target=$(readlink "${target}")
        cd "$(dirname "${target}")" || fail "Bad directory: ${target}"
        target=$(basename "${target}")
    done

    # Compute the canonicalized name by finding the physical path
    # for the directory we're in and appending the target file.
    local dir
    local result

    dir="$(pwd -P)"
    result="$(echo "${dir}/${target}" | sed -e 's|/.$||')"

    echo "${result}"
}
