# shellcheck shell=bash

# Common Defines and Libraries
# ------------------------------------------------------------------------------
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/src-paths.sh"

# Configuration Information
# ------------------------------------------------------------------------------
export GITLAB_CLUSTER_RC="${GITLAB_CLUSTER_RC:-${SECRETS_PATH}/gitlab-cluster-rc.sh}"
if [ -f "${GITLAB_CLUSTER_RC}" ]; then
    echo "Loading configuration from ${GITLAB_CLUSTER_RC}"
    set -a
    . "${GITLAB_CLUSTER_RC}"
    set +a
fi

. "${SRC_PATH}/scripts/deploy/lib/rc-helpers.sh"

SSH_KNOWN_HOSTS_FILE="${SSH_KNOWN_HOSTS_FILE:-${ARTIFACTS_PATH}/known_hosts}"
mkdir -p "$(dirname "${SSH_KNOWN_HOSTS_FILE}")"

GOOGLE_JSON_FILE="${GOOGLE_JSON_FILE:-google-credentials.json}"
GOOGLE_APPLICATION_CREDENTIALS="${SECRETS_PATH}/${GOOGLE_JSON_FILE}"

GITLAB_CLUSTER_DESCRIPTION_JSON="${GITLAB_CLUSTER_DESCRIPTION_JSON:-gitlab-cluster.json}"

private_key="$(select_ssh_key "${SSH_KEY_NAME}")"
public_key="${private_key}.pub"

SSH_PRIVATE_KEY="$(cat "${private_key}")"
SSH_PUBLIC_KEY="$(cat "${public_key}")"
export SSH_PRIVATE_KEY
export SSH_PUBLIC_KEY

SSH_USER="${SSH_USER:-ansible}"

VAULT_PASSWORD_FILE="${VAULT_PASSWORD_FILE:-vault-password-file}"
VAULT_PASSWORD_PATH="${SECRETS_PATH}/${VAULT_PASSWORD_FILE}"
GITLAB_LICENSE_FILE="${GITLAB_LICENSE_FILE:-GitLab.gitlab-license}"
GITLAB_LICENSE_PATH="${SECRETS_PATH}/${GITLAB_LICENSE_FILE}"
TF_BACKEND_CONFIG_FILE="${TF_BACKEND_CONFIG_FILE:-backend.tf}"
TF_BACKEND_CONFIG_PATH="${TF_BACKEND_CONFIG_PATH:-${SRC_PATH}/terraform/${TF_BACKEND_CONFIG_FILE}}"

# The following items are REQUIRED and have no defaults
validate_variable "consul_pgbouncer_password"
validate_variable "application_pgbouncer_password"
validate_variable "application_database_password"
validate_variable "TERRAFORM_PREFIX"
validate_variable "GCLOUD_ZONE"
validate_variable "BACKEND"

case "${BACKEND}" in
  "gcs" )
    validate_variable "GCS_PREFIX"
    validate_variable "STATE_GCS_BUCKET"
    ;;
  "gitlab" )
    validate_variable "GITLAB_API_USERNAME"
    validate_variable "GITLAB_API_TOKEN"
    validate_variable "GITLAB_API_V4_URL"
    validate_variable "GITLAB_PROJECT_NAME"
    validate_variable "GITLAB_PROJECT_ID"
    ;;
  * )
    fail "Please set the 'BACKEND' variable to one of the following: gcs, gitlab"
    ;;
esac


# Database cluster manager
DATABASE_CLUSTER_MANAGER="${DATABASE_CLUSTER_MANAGER:-repmgr}"
validate_database_cluster_manager "${DATABASE_CLUSTER_MANAGER}"

# General Configuration
# ------------------------------------------------------------------------------
export ORCHESTRATION_USER="${SSH_USER:-ansible}"
export QA_IP_FILE="${QA_IP_FILE:-${SRC_PATH}/rc_gitlab_application_public_ip_address}"
export GITLAB_PROVISIONING_DATA="${GITLAB_PROVISIONING_DATA:-${SRC_PATH}/gitlab_provisioning_data.json}"
export SSH_KNOWN_HOSTS_FILE="${SSH_KNOWN_HOSTS_FILE:-${HOME}/.ssh/known_hosts}"
export KEYSCAN_WAIT_SECONDS="${KEYSCAN_WAIT_SECONDS:-0}"

export GITLAB_CLUSTER_DESCRIPTION="${SECRETS_PATH}/${GITLAB_CLUSTER_DESCRIPTION_JSON}"

# Package Configuration
PACKAGE_SOURCE="${PACKAGE_SOURCE:-REPO}"
PACKAGE_SOURCE="$(echo "${PACKAGE_SOURCE}" | tr '[:lower:]' '[:upper:]')"

if [ -f "${GITLAB_CLUSTER_DESCRIPTION}" ]; then
  distro_name=$(jq -r '.operating_system_name as $x | if $x == null then "ubuntu" else $x end | ascii_downcase' "${GITLAB_CLUSTER_DESCRIPTION}")
fi

validate_package_source "${PACKAGE_SOURCE}" "${PACKAGE_URL}" "${PACKAGE_TOKEN}" "${distro_name}" "${PACKAGE_VARIANT}"

# Google Cloud Credentials
# ------------------------------------------------------------------------------
export GOOGLE_APPLICATION_CREDENTIALS="${GOOGLE_APPLICATION_CREDENTIALS:-${SRC_PATH}/google-credentials.json}"

if [ -n "${GCLOUD_CREDENTIALS_JSON}" ]; then
    printf "%s" "${GCLOUD_CREDENTIALS_JSON}" > "${GOOGLE_APPLICATION_CREDENTIALS}"
fi

if [ -z "${GCLOUD_PROJECT}" ]; then
    if [ ! -f "${GOOGLE_APPLICATION_CREDENTIALS}" ]; then
        fail "Cannot find ${GOOGLE_APPLICATION_CREDENTIALS}, see documentation on GOOGLE_JSON_FILE"
    fi
    GCLOUD_PROJECT=$(jq -r .project_id < "${GOOGLE_APPLICATION_CREDENTIALS}")
fi
export GCLOUD_PROJECT

if [ -z "${GCLOUD_CLIENT_EMAIL}" ]; then
    if [ ! -f "${GOOGLE_APPLICATION_CREDENTIALS}" ]; then
        fail "Cannot find ${GOOGLE_APPLICATION_CREDENTIALS}, see documentation on GOOGLE_JSON_FILE"
    fi
    GCLOUD_CLIENT_EMAIL=$(jq -r .client_email < "${GOOGLE_APPLICATION_CREDENTIALS}")
fi
export GCLOUD_CLIENT_EMAIL

# Site provider - default to Google if not provided.
export SITE_PROVIDER="${SITE_PROVIDER:-google}"
export TF_VAR_SITE_PROVIDER="${SITE_PROVIDER}"

# Object storage - default to Google if not provided.
export OBJECT_STORAGE_PROVIDER="${OBJECT_STORAGE_PROVIDER:-google}"

# Certificates
# ------------------------------------------------------------------------------
CERT_PROVIDER="${CERT_PROVIDER:-generated}"
CERT_PROVIDER="$(echo "${CERT_PROVIDER}" | tr '[:upper:]' '[:lower:]')"
export CERT_PROVIDER

CERTS_PATH="${CERTS_PATH:-${ARTIFACTS_PATH}/certs/${TERRAFORM_PREFIX}}"
export CERTS_PATH
mkdir -p "${CERTS_PATH}"
