# shellcheck shell=bash

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/src-paths.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

select_ssh_key() {
    local key_name="${1}"

    key_name="${key_name:-gitlab-orchestrator}"
    # The priority order to load a key is:
    # - Directly Mounted Key
    local mount_key_path="${SRC_PATH}/${MOUNTED_SSH_KEY_DIR}/${key_name}"
    # - Key in the mounted host key directory
    local host_key_path="${SRC_PATH}/${HOST_SSH_KEY_DIR}/${key_name}"
    # - Dynamically generated key
    local dynamic_key_path="${HOME}/.ssh/${key_name}"

    local ssh_key
    if [ -f "${mount_key_path}" ]; then
        ssh_key="${mount_key_path}"
    elif [ -f "${host_key_path}" ]; then
        ssh_key="${host_key_path}"
    else
        ssh_key="${dynamic_key_path}"

        if [ ! -f "${ssh_key}" ]; then
            ssh_dir="$(dirname "${ssh_key}")"
            mkdir -p "${ssh_dir}"
            chmod 0700 "${ssh_dir}"
            ssh-keygen -b 4096 -t rsa -f "${ssh_key}" -q -N ""
        fi
    fi

    echo "${ssh_key}"
}

validate_package_source() {
  local package_source="${1}"
  local package_url="${2}"
  local package_token="${3}"
  local distro_name="${4}"
  local package_variant="${5}"

  case "${package_variant}" in
    ee|ce)
      ;;
    *)
      if [ ! "${package_variant}" = "" ]; then
        fail "No such variant '${package_variant}', select from 'ee' or 'ce'."
      fi
      ;;
  esac

  case "${package_source}" in
    URL|GITLAB_JOB|NIGHTLY|REPO)
      ;;
    *)
      fail "${package_source} is not a valid package source"
      ;;
  esac

  if [ "${package_source}" = "NIGHTLY" ] && [ "${distro_name}" != "ubuntu" ]; then
    fail "Currently there are only nightly builds for Ubuntu"
  fi

  if [ -n "${package_url}" ]; then
    case "${package_source}" in
      URL|GITLAB_JOB)
        ;;
      *)
        fail "Conflicting package choices: PACKAGE_SOURCE is not URL or GITLAB_JOB but PACKAGE_URL is set"
        ;;
    esac
  fi

  if [ "${package_source}" = "URL" ]; then
    if [ -z "${package_url}" ]; then
      fail "If PACKAGE_SOURCE is URL, then PACKAGE_URL must be set"
    fi

    if ! curl -sSL -I -D - "${package_url}" -o /dev/null 2>&1 | grep -q "200 OK"; then
      fail "${package_url} is not available, check URL"
    fi
  fi

  if [ "${package_source}" = "GITLAB_JOB" ]; then
    if [ "${distro_name}" != "ubuntu" ]; then
      fail "Currently pipelines only build for Ubuntu"
    fi

    if [ -z "${package_token}" ]; then
      fail "Source ${package_source} requires PACKAGE_TOKEN for API download access"
    fi

    if [ -z "${package_url}" ]; then
      fail "Source ${package_source} requires PACKAGE_URL for API download"
    fi

    if ! curl -sSL -I -D - --header "PRIVATE-TOKEN: ${package_token}" "${package_url}" -o /dev/null 2>&1 | grep -q "HTTP/2 200"; then
      fail "Could not download package from pipeline"
    fi
  fi
}

validate_database_cluster_manager() {
    local solution="${1}"
    [ "${solution}" = 'repmgr' -o "${solution}" = 'patroni' ] \
        || fail 'DATABASE_CLUSTER_MANAGER can be either `patroni` or `repmgr`.'
}
