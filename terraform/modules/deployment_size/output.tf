################################################################################
# Deployment Size
# Module Outputs
################################################################################

output "deployment_resources" {
  description = "Data structure describing all nodes required for a deployment"
  value       =  flatten([
    for site_name, deployment_size in local.site_deployment_sizes: [
      for resource_entry in lookup(local.resource_list_lookup_table, deployment_size, []) :
      merge(resource_entry, { "site_name" = site_name })
    ]
  ])
}

output "object_storage" {
  description = "Boolean value indicating whether object storage should be enabled"
  value       = contains(flatten([
    for site, deployment_size in local.site_deployment_sizes: [
      lookup(local.object_storage_lookup_table, deployment_size, false)
    ]
  ]), true)
}
