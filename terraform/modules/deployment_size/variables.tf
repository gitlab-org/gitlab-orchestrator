################################################################################
# Deployment Size
# Module Inputs
################################################################################

variable "site_provider" {
  type        = string
  description = "The service that terraform will use to generate resources"
  default     = "google"
}

variable "deployment_sites" {
  type        = map(string)
  description = "Map with the format { site_name = deployment_size, ... }"
}
