################################################################################
# CentOS Cloud Provider Image Definitions
################################################################################
locals {
  centos = {
    "default" = {
      "google" = "centos-cloud/centos-8"
    },
    "8" = {
      "google" = "centos-cloud/centos-8"
    },
    "7" = {
      "google" = "centos-cloud/centos-7"
    },
    "6" = {
      "google" = "centos-cloud/centos-6"
    }
  }
}
