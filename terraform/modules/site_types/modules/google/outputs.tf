output "hostnames" {
  value = google_compute_instance.google_cloud_vm[*].name
  description = "Host names of google compute instances"
}

output "internal_addresses" {
  value = google_compute_instance.google_cloud_vm[*].network_interface[0].network_ip
}

output "external_addresses" {
  value = google_compute_instance.google_cloud_vm[*].network_interface[0].access_config[0].nat_ip
}

output "object_storage_buckets" {
  value = flatten([
    google_storage_bucket.artifacts_object_storage[*].url,
    google_storage_bucket.external_diffs_object_storage[*].url,
    google_storage_bucket.lfs_objects_object_storage[*].url,
    google_storage_bucket.uploads_object_storage[*].url,
    google_storage_bucket.packages_object_storage[*].url,
    google_storage_bucket.dependency_proxy_object_storage[*].url,
    google_storage_bucket.terraform_state_object_storage[*].url
  ])
}
