resource "google_storage_bucket" "artifacts_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-artifacts"
  location      = "US"
  force_destroy = true
}

resource "google_storage_bucket" "external_diffs_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-external-diffs"
  location      = "US"
  force_destroy = true
}

resource "google_storage_bucket" "lfs_objects_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-lfs-objects"
  location      = "US"
  force_destroy = true
}

resource "google_storage_bucket" "uploads_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-uploads"
  location      = "US"
  force_destroy = true
}

resource "google_storage_bucket" "packages_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-packages"
  location      = "US"
  force_destroy = true
}

resource "google_storage_bucket" "dependency_proxy_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-dependency-proxy"
  location      = "US"
  force_destroy = true
}

resource "google_storage_bucket" "terraform_state_object_storage" {
  count         = var.object_storage ? 1 : 0
  name          = "${var.site_label}-terraform-state"
  location      = "US"
  force_destroy = true
}
