################################################################################
# Node Technical Specifications
################################################################################

variable "deployment_resources" {
  description = "stuff"
}

variable "site_label" {
  description = "cluster identifier creating namespace"
}

variable "os_image" {
  description = "The underlaying operating system that powers the node"
}

variable "object_storage" {
  description = "Boolean value indicating whether object storage should be enabled"
  default     = false
}

################################################################################
# Remote Management Information
################################################################################

variable "ssh_user" {
  description = "Remote access user account"
  default = "ansible"
}

variable "ssh_public_key" {
  description = "SSH public key for remote access"
}
