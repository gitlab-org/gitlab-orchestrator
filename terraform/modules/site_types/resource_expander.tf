locals {
  # expand a resource map object and inject an equal number of entries to
  # the resource_count value
  # [{ "resource_count" = 3 }]
  # [{ "resource_count" = 3, "resource_index" = 1" }, ... ]
  expanded_resources    = flatten([
      for resource_type in var.deployment_resources: [
        for i in range(resource_type["resource_count"]):
        merge(resource_type, { "resource_index" = i + 1 })
      ]
  ])
}
