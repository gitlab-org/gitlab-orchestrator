locals {
  google_resources = [
    for resource_type in local.expanded_resources:
    resource_type
    if resource_type.site_provider == "google"
  ]
}
