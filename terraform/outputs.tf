output "hostnames" {
  value = module.sites.hostnames
}

output "external_addresses" {
  value = module.sites.external_addresses
}

output "internal_addresses" {
  value = module.sites.internal_addresses
}

output "object_storage_buckets" {
  value = module.sites.object_storage_buckets
}

output "deployment_sites" {
  value = var.deployment_sites
}
