variable "ssh_user" {
  description = "Username to allow ssh in with"
}

variable "geo" {
  description = "Attribute dictionary describing a geo cluster"
  default = {
    "enabled": "false"
  }
}

variable "ssh_public_key" {
  description = "ssh public_key to use for ssh_user"
}

variable "prefix" {
  description = "cluster identifier creating namespace"
}

variable "google_project" {
  description = "Google Project ID"
}

variable "google_zone" {
  description = "Google zone to provision resources in"
  default     = "us-central1-b"
}

locals {
  google_region = join("-", slice(split("-", var.google_zone), 0, 2))
}

variable "operating_system_name" {
  description = "the colloquial name of the operating system"
  default = ""
}

variable "operating_system_version" {
  description = "the version number of the operating system"
  default = ""
}

variable "deployment_sites" {
    description = "map representing the site or sites that will be deployed and their names as a map"
    default     = {
      "gitlab"  = "omnibus_pipeline"
    }
}
